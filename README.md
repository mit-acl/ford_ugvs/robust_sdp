This is a fork of the [RobustNN repo](https://github.com/arobey1/RobustNN) with a couple minor changes (e.g., add `setup.py`).

# RobustNN

Python implementation of the SDP described in "Safety Verification and Robustness Analysis of Neural Networks via Quadratic Constraints and Semidefinite Programming" (<https://arxiv.org/abs/1903.01287>).

```
python main.py
```
